package ir.syborg.app.car;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;


public class Bently extends Activity {

    MediaPlayer sound;


    @Override
    protected void onPause() {
        super.onPause();
        sound.stop();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bently);

        sound = MediaPlayer.create(Bently.this, R.raw.bentley);

        Button btnNxt = (Button) findViewById(R.id.btnNxt);
        Button btnPrv = (Button) findViewById(R.id.btnPrv);
        Button btnPly = (Button) findViewById(R.id.btnPly);
        Button btnStp = (Button) findViewById(R.id.btnStp);

        btnPly.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                sound.start();
            }
        });
        btnStp.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                sound.pause();

            }
        });

        btnNxt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(Bently.this, Ferrari.class);
                startActivity(intent);
                finish();

            }
        });

        btnPrv.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(Bently.this, Porsche.class);
                startActivity(intent);
                finish();

            }
        });
    }
}