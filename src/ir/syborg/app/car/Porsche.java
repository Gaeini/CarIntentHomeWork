package ir.syborg.app.car;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;


public class Porsche extends Activity {

    MediaPlayer sound;


    @Override
    protected void onPause() {
        super.onPause();
        sound.stop();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater findItem = getMenuInflater();
        findItem.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.porsche);

        sound = MediaPlayer.create(Porsche.this, R.raw.porsche);

        Button btnNxt = (Button) findViewById(R.id.btnNxt);
        Button btnPrv = (Button) findViewById(R.id.btnPrv);
        Button btnPly = (Button) findViewById(R.id.btnPly);
        Button btnStp = (Button) findViewById(R.id.btnStp);

        btnPly.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                sound.start();
            }
        });
        btnStp.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                sound.pause();

            }
        });

        btnNxt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(Porsche.this, Bently.class);
                startActivity(intent);
                finish();

            }
        });

        btnPrv.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(Porsche.this, Ferrari.class);
                startActivity(intent);
                finish();

            }
        });
    }

}