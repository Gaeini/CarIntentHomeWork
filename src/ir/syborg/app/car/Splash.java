package ir.syborg.app.car;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;


public class Splash extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        Thread timer = new Thread() {

            @Override
            public void run() {
                super.run();
                try {
                    sleep(3000);
                    Log.i("Man", "After 3 sec");
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
                finally {
                    Intent intent = new Intent(Splash.this, Porsche.class);
                    startActivity(intent);
                    finish();

                }
            }

        };
        timer.start();
    }

}
